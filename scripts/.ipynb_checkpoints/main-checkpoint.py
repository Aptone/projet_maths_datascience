# Importation des modules
import pandas as pd

import fonct.valeurManquante as valM
import fonct.rapportDataFrame as rdf
import fonct.traitementData as tdata

#####################
# Importation des csv

dataPolitique = pd.read_csv("../data/data.csv")
dataPaysGeoID = pd.read_csv("../data/countries.csv")
dataTestCovidWord = pd.read_csv("../data/tested_worldwide.csv")
dataCovidTestObservation = pd.read_csv("../data/covid-testing-all-observations.csv")
dataCovid = pd.read_csv("../data/owid-covid-data.csv")
dataCovidGlobal = pd.read_csv("../data/coronavirus.politologue.com-pays-2020-11-05.csv")

dataConsommation = pd.read_csv("../data/Consommation.csv")
dataConsoLevel = pd.read_csv("../data/consommation-level.csv")
dataConsoEvol = pd.read_csv("../data/evolution-consommation.csv")
dataConsoRep = pd.read_csv("../data/repartition-consommation.csv")

#####################
# Bilan des CSV

dataPolitique.head()
rdf.rapDataFrame(dataPolitique)
# Utilisation de la méthode pour faire un rapport sur les données manquantes
valM.pourcValeurManquante(dataCovid)
dataConsoRep.head()
valM.valeurManquante(dataTestCovidWord)
rdf.rapDataFrame(dataConsoRep)
valM.valeurManquante(dataCovidTestObservation)
rdf.rapDataFrame(dataTestCovidWord)
valM.valeurManquante(dataTestCovidWord)

#####################
# Traitement des CSV

# Traitement des valeurs manquantes
# Comme nous le remarqu'on beaucoup de colonne manque de valeur, cela peut s'expliquer par le fait que la valeur soit a 0. Par exemple en janvier 2020 il y avait très peut de contapmination et de test de covid dans les pays d'Europe c'est pour cela que la valeur valeur est a nan. Pour eviter d'avoir des NaN nous allons donc les remplacer par 0.
# Remplacement les valeurs NaN par 0
tdata.replaceNan(dataTestCovidWord)

# Colonne a garder
dataPolitique = dataPolitique[['dateRep','day','month','year','cases','deaths','countriesAndTerritories','geoId','countryterritoryCode','popData2019','continentExp','Cumulative_number_for_14_days_of_COVID-19_cases_per_100000']]
dataTestCovidWord = dataTestCovidWord[['Date','Country_Region','Province_State','positive','active','hospitalized','recovered','death','total_tested','daily_tested','daily_positive']]
dataCovid = dataCovid[['iso_code','continent','location','date','total_cases','new_cases','total_deaths','new_deaths','hosp_patients_per_million','total_tests_per_thousand','new_tests_per_thousand','positive_rate','population','population_density','median_age','aged_65_older','aged_70_older','extreme_poverty','cardiovasc_death_rate','diabetes_prevalence','life_expectancy','human_development_index']]
dataConsommation = dataConsommation[['Unnamed: 0','Unnamed: 1','Designations des regroupements','rations','oct. 19','juil. 20','ao√±t 2020','sept. 20','oct. 20']]
dataConsoRep = dataConsoRep[['DATE','Total_Rep','Food products_Rep','Food products except tobacco_Rep','Engineered products_Rep','Transport equipment_Rep','Household durables_Rep','Textile-leather_Rep','Energy_Rep','Energy, water, waste collection and treatment_Rep','Fuel and oil_Rep']]
dataConsoLevel = dataConsoLevel[['DATE','Total_Level','Food products','Food products except tobacco_Level','Engineered products_Level','Transport equipment_Level','Household durables_Level','Textile-leather_Level','Other engineered goods','Energy_Level','Energy, water, waste collection and treatment_Level','Fuel and oil_Level']]
dataConsoEvol = [['DATE','Total_Evol','Food products_Evol','Food products except tobacco_Evol','Engineered products_Evol''Transport equipment_Evol','Household durables_Evol','Textile-leather_Evol','Other engineered goods_Evol','Energy_Evol','Energy, water, waste collection and treatment_Evol','Fuel and oil_Evol']]

## Suppression des colonnes où il manque des données plus de 32% des valeurs
dataCovid = tdata.cleanData(dataCovid)

# Renommer colonnes pour merge
# dataPolitique.rename(columns={'countryterritoryCode': 'ISO code'},inplace=True)
tdata.renameColonne(dataPolitique, 'countryterritoryCode', 'ISO code')
tdata.renameColonne(dataTestCovidWord, 'Country_Region', 'countriesAndTerritories')
tdata.renameColonne(dataCovid, 'iso_code', 'ISO code')

# Uniformisation des Dates
dataTestCovidWord['Date'] = dataTestCovidWord['Date'].str.replace('-', '/')
dataTestCovidWord.head()
# TODO
# Fonction Uniformiser date
# A CHANGER POUR dataTestCovidWord et dataCovid
dataTestCovidWord['Date'].dtype
dataTestCovidWord['Date'] = pd.to_datetime(dataTestCovidWord['Date'], format="%Y/%m/%d")
dataTestCovidWord['Date'].dtype
dataTestCovidWord.head()

# A CHANGER POUR dataCovid

#####################
# Merge DataFrame

# Consomation
#dataFrameP = tdata.mergeDataFrame(dataConsoLevel, dataConsoEvol, 'DATE')
#dataFrameFinalConso = tdata.mergeDataFrame(dataFrameP, dataConsoRep, 'DATE')

# Covid
dataFrame = pd.merge(dataCovidTestObservation, dataPolitique, on='ISO code')
print("OK -- dataFrame ")
dataFrameS = pd.merge(dataFrame, dataTestCovidWord, on='countriesAndTerritories')
print("OK -- dataFrameS ")
#dataFrameS.shape
dataFrameFinalCovid = pd.merge(dataFrameS, dataCovid, on='ISO code')
print("OK -- dataFrameFinalCovid ")
#dataFrameFinalCovid.shape

#####################
# Exportation du DataFrame final en CSV

# Cela nous permettra de ne pas réexécuter le code qui est très long pour les merges

dataFrameFinalCovid.to_csv(r'../data/FinalDATAFRAMECovid.csv',index=False)
# dataFrameFinalConso.to_csv(r'../data/FinalDATAFRAMEConsomation.csv',index=False)
