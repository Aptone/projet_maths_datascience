import numpy as np
import pandas as pd


# Rapport des valeurs manquantes
def valeurManquante(dataFrame):
    # Contage des données manquantes par colonne
    valeur_manquantes_count = dataFrame.isnull().sum()
    print("CONTAGE DES VALEURS MANQUANTES PAR COLONNE\n", valeur_manquantes_count)

    # Total du nombre de données manquantes
    total_cellules = np.product(dataFrame.shape)
    total_donnee_manquante = valeur_manquantes_count.sum()
    print("\nTOTAL DE DONNEE MANQUANTE : ", total_donnee_manquante)

    # Pourcentage de donnée manquantes
    pourcentage_donnee_manquantes = (total_donnee_manquante / total_cellules) * 100
    print("\nPOURCENTAGE DE DONNEE MANQUANTE : ", pourcentage_donnee_manquantes)


# Pourcentage de donnée manquante par col
def pourcValeurManquante(dataFrame):
    # Tableau avec pourcentage de donnée manquantes
    total = dataFrame.isnull().sum().sort_values(ascending=False)
    percent = (dataFrame.isnull().sum() / dataFrame.isnull().count()).sort_values(ascending=False)
    missing_data = pd.concat([total, percent], axis=1, keys=['Total', 'Percent'])
    # axis=1 suppression des colonne (=0 sup ligne)
    return print(missing_data.head(60))
