import pandas as pd


# Remplace les valeurs NaN par 0
def replaceNan(dataFrame):
    dataFrame.fillna(0, inplace=True)


# Suppression des colonnes ou il manque des donnees plus de 32% des valeurs
def cleanData(x_dataFrame):
    # Tableau avec pourcentage de donnée manquantes
    total = x_dataFrame.isnull().sum().sort_values(ascending=False)
    percent = (x_dataFrame.isnull().sum() / x_dataFrame.isnull().count()).sort_values(ascending=False)
    missing_data = pd.concat([total, percent], axis=1, keys=['Total', 'Percent'])
    newDataFrame = x_dataFrame.drop((missing_data[missing_data['Percent'] > 0.32]).index, 1)
    print("Delete ok")
    return newDataFrame


# Suppression colonnes
def supColonne(dataFrame, ColName):
    dataFrame.drop(ColName, axis=1, inplace=True)


# Rename colonne
def renameColonne(dataFrame, colonneAChanger, newName):
    dataFrame.rename(columns={colonneAChanger: newName}, inplace=True)


# Merge data
def mergeDataFrame(firstDataFrameSelect, segondDataFrame, key):
    newDataFrame = pd.merge(firstDataFrameSelect, segondDataFrame, on=key)
    print("Merge OK")
    return newDataFrame

# Uniformisation Date


# ajoute valeur d'un dataframe a un autre dataframe si la valeur est absente
# Copie du data frame
# dataPolitiquetest = dataPolitique
# Boucle qui permet de rajouter les id au pays au valeurs manquante
# for index, line in dataPolitique.iterrows():
#    if pd.isna(line.geoId) :
#        #print(line.countriesAndTerritories)
#        for j, dataPays in dataPaysGeoID.iterrows() :
#            if line.countriesAndTerritories == dataPays['name'] :
#                #print(dataPays['name'] + ' retrouvé')
#                #dataPolitique[index, 'geoId'] = dataPays.code
#                line.geoId = dataPays['code']

# Ajouter les valeurs countryterritoryCode
# for index, line in dataPolitique.iterrows():
#    if pd.isna(line.countryterritoryCode) :
#        print(index)
#        for j, line2 in dataCovid.iterrows() :
#            if line.countriesAndTerritories == line2.location:
#                print(line2)
# line.countryterritoryCode = dataCovid.iso_code
